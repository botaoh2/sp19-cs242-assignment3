import React, { Component } from 'react';
import { StyleSheet, Animated, Text, TouchableHighlight, FlatList, View, Linking } from 'react-native';
import FadeInView from './FadeInView'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
    },
    item: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'space-evenly',
        height: 120,
        borderColor: '#e5e5e5',
        borderBottomWidth: 2,
        borderTopWidth: 2,
        paddingLeft: 15,
        paddingRight: 15,
    },
    header: {
        height: 40,
        alignItems:'center',
        justifyContent: 'center',
        borderColor: '#e5e5e5',
        borderBottomWidth: 2,
    }
});

export default class Repos extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.loadRepoList()
    }

    componentDidMount() {
        this.mounted = true
    }

    componentWillUnmount() {
        this.mounted = false
    }

    render() {
        return (
            <View style={styles.container}>
            <FadeInView>
                <View style={styles.header}>
                    <Text style={{fontWeight: 'bold', fontSize: 25}}>{this.props.user}'s Repositories:</Text>
                </View>
                <FlatList
                style={{backgroundColor: '#e5e5e5'}}
                data={this.state.repoList}
                renderItem={this._renderItem}
                ListHeaderComponent={this._header}
                ></FlatList>
            </FadeInView>
            </View>
        );
    }

    _renderItem({item}) {
        return (
            <TouchableHighlight
                onPress={() => {
                    Linking.openURL(item.html_url)
                }}
            >
            <View style={styles.item}>
            <Text style={{fontWeight:'bold'}}>{item.name}</Text>
            <Text>Owner: {item.owner}</Text>
            <Text>Description: {item.description || "Nothing to show here."}</Text>
            </View>
            </TouchableHighlight>
        )
    }

    loadRepoList() {
        fetch(`https://api.github.com/users/${this.props.user}/repos`)
            .then((response) => response.json())
            .then((responseJson) => {
                if(!this.mounted) {
                    return
                }
                this.setState({
                    repoList: responseJson.map(item => {
                        return {
                            key: item.name,
                            name: item.name,
                            owner: item.owner.login,
                            description: item.description,
                            html_url: item.html_url,
                        }
                    })
                })
            }).catch((error) => {
                this.setState({repoList: []})
            });
    }

}
