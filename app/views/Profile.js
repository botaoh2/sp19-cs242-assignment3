import React, { Component } from 'react';
import { StyleSheet, Text, Dimensions, View, Image } from 'react-native';
import FadeInView from './FadeInView'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    name: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 40,
    },
});

function getOrientation() {
    const {width, height} = Dimensions.get('window')
    return width < height ? 'portrait' : 'landscape'
}

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.handleOrientationChange = this.handleOrientationChange.bind(this)
        this.state = {
            name: "Loading",
            pic: "https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif",
            login: "",
            bio: "",
            public_repos: 0,
            followers: 0,
            following: 0,
            createDate: "",
            orientation: getOrientation(),
        }
        this.loadProfile()
    }

    // https://tutorialscapital.com/dynamically-find-device-screen-orientation-portrait-or-landscape-react-native-tutorial/
    componentDidMount() {
        this.mounted = true
        Dimensions.addEventListener('change', this.handleOrientationChange);
    }

    componentWillUnmount() {
        this.mounted = false
        Dimensions.removeEventListener('change', this.handleOrientationChange);
    }

    handleOrientationChange() {
        this.setState({
            orientation: getOrientation()
        })
    }

    render() {
        return (
            <FadeInView style={styles.container}>
                <View style={{alignItems:'center', flexDirection:this.state.orientation == 'portrait' ? 'column': 'row'}}>
                    <Image source={{uri:this.state.pic}} style={{width:150, height:150, margin: 10}}/>
                    <View style={{alignItems:'center'}}>
                        <Text style={styles.name}>{this.state.name}</Text>
                        <Text style={{fontSize:30}}>{this.state.login}</Text>
                        <Text style={{fontSize:30}}>{this.state.bio}</Text>
                    </View>
                </View>
                <View>
                    <Text style={{fontSize:30}}>Repositories:&nbsp;
                        <Text onPress={()=>{this.props.handleSwitchTab('Repos')}}>{this.state.public_repos}</Text>
                    </Text>
                    <Text style={{fontSize:30}}>Followers:&nbsp;
                        <Text onPress={()=>{this.props.handleSwitchTab('Followers')}}>{this.state.followers}</Text>
                    </Text>
                    <Text style={{fontSize:30}}>Following:&nbsp;
                        <Text onPress={()=>{this.props.handleSwitchTab('Following')}}>{this.state.following}</Text>
                    </Text>
                    <Text style={{fontSize:30}}>Create Date: {this.state.createDate}</Text>
                </View>
            </FadeInView>
        );
    }

    loadProfile() {
        fetch(`https://api.github.com/users/${this.props.user}`)
            .then((response) => response.json())
            .then((responseJson) => {
                if(!this.mounted) {
                    return
                }
                this.setState({
                    name: responseJson.name,
                    pic: responseJson.avatar_url,
                    login: responseJson.login,
                    bio: responseJson.bio,
                    public_repos: responseJson.public_repos,
                    followers: responseJson.followers,
                    following: responseJson.following,
                    createDate: responseJson.created_at.slice(0, 10),
                })
            }).catch((error) => {
                this.setState({
                    name: `Could not find user: ${this.props.user}`
                })
            });
    }
}
