import React, { Component } from 'react';
import { StyleSheet, Text, Button, View, Alert } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: 50,
        flexDirection: 'row',
        alignItems: 'stretch',
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //TODO remove border
        borderColor: '#636363',
        borderTopWidth: 0.5,
    },
});

class Tab extends Component {

    render() {
        let name = this.props.name
        let isActive = name == this.props.activeTab
        return (
            <View style={styles.tab}>
            <Button
            color={isActive ? 'red' : '#636363'}
            onPress={() => {
                this.props.handleSwitchTab(name)
            }}
            title={name}/>
            </View>
        )
    }

}

export default class TabBar extends Component {

    constructor(props) {
        super(props)
        this.handleSwitchTab = this.handleSwitchTab.bind(this)
    }

    handleSwitchTab(newTab) {
        this.props.handleSwitchTab(newTab)
    }

    render() {
        return (
            <View style={styles.container}>
                <Tab name="Profile" activeTab={this.props.activeTab} handleSwitchTab={this.handleSwitchTab}/>
                <Tab name="Repos" activeTab={this.props.activeTab} handleSwitchTab={this.handleSwitchTab}/>
                <Tab name="Followers" activeTab={this.props.activeTab} handleSwitchTab={this.handleSwitchTab}/>
                <Tab name="Following" activeTab={this.props.activeTab} handleSwitchTab={this.handleSwitchTab}/>
            </View>
        )
    }

}
