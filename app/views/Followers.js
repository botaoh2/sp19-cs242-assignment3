import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, Image } from 'react-native';
import FadeInView from './FadeInView'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
});

export default class Followers extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <FadeInView style={styles.container}>
                <Text>follower</Text>
            </FadeInView>
        );
    }

}
