import React, { Component } from 'react';
import { Animated } from 'react-native';

export default class FadeInView extends Component {
    state = {
        fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
    }

    componentDidMount() {
        this.fadeAnimation = Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 1,
                duration: 500,
            }
        )
        this.fadeAnimation.start();
    }

    componentWillUnmount() {
        this.fadeAnimation.stop();
    }

    render() {
        let { fadeAnim } = this.state;

        return (
            <Animated.View                 // Special animatable View
            style={{
                ...this.props.style,
                opacity: fadeAnim,         // Bind opacity to animated value
            }}
            >
            {this.props.children}
            </Animated.View>
        );
    }
}
