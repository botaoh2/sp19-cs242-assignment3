import React, { Component } from 'react';
import { StyleSheet, Text, SafeAreaView, View, Image } from 'react-native';
import Profile from './views/Profile'
import Repos from './views/Repos'
import Followers from './views/Followers'
import Following from './views/Following'
import TabBar from './views/Tab'

const user = "octocat"

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: "Profile",
        }
        this.handleSwitchTab = this.handleSwitchTab.bind(this)
    }

    handleSwitchTab(newTab) {
        this.setState({activeTab: newTab})
    }

    render() {
        let tabPage = ""
        switch(this.state.activeTab) {
            case "Profile":
                tabPage = (<Profile user={user} handleSwitchTab={this.handleSwitchTab}/>)
                break
            case "Repos":
                tabPage = (<Repos user={user}/>)
                break
            case "Followers":
                tabPage = (<Followers/>)
                break
            case "Following":
                tabPage = (<Following/>)
                break
        }
        return (
            <SafeAreaView style={{flex:1}}>
                <View style={styles.container}>
                {tabPage}
                <TabBar activeTab={this.state.activeTab} handleSwitchTab={this.handleSwitchTab}></TabBar>
                </View>
            </SafeAreaView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'stretch',
    },
});
